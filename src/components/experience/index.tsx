import styled from 'styled-components'
const Experience = () => {
  const ExperienceStyle = styled.div`
    .experience-content {
      padding: 20px;
    }
    .header-mian {
      font-size: 24px;
      font-weight: 700;
    }
    .header {
      font-size: 16px;
    }
    .content {
      font-size: 18px;
    }
    .img {
      width: 80px;
    }
    .discription {
      margin-top: 10px;
    }
  `
  return (
    <ExperienceStyle>
      <br />
      <div className="title mt-5">EXPERIENCE</div>
      <div className="experience-content">
        <div className="header-mian">
          INTERNSHIP
          <span className="header" style={{ float: 'right' }}>
            Digio - Thailand Co., Ltd. <br />
            <span className="header" style={{ float: 'right' }}>
              Nov 2021 - Feb 2022
            </span>
          </span>
        </div>
        <br />
        <br />
        <div className="content">
          <div className="row">
            <div className="col-sm-1">
              <img src="/experience/email.png" className="img" />
            </div>
            <div className="col-sm-11 discription">
              <b>
                Email Service: Develop an email system to deliver data to
                customers, which reads information from CSV files located in
                SFTP. The system will have scheduled intervals to continuously
                read files. Once the file is found, the system will send an
                email to the customer.
              </b>
              <br />
              <br />
              - Develop the system using Node.js framework. <br />
              - Utilize Redis for temporary data storage, as some data may not
              need to be stored permanently. <br />- MySQL database.
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-sm-1 mt-3">
              <img src="/experience/robinhood.png" className="img" />
            </div>
            <div className="col-sm-11 mt-3 discription">
              <b>
                Robinhood Product Company Website: Enhance the website for the
                company's Robinhood product by integrating various functions
                through APIs.
              </b>
              <br />
              <br />- Design and develop the front-end web interface using
              Laravel, HTML, CSS, AJAX and JavaScript. <br /> - Develop backend
              system using Laravel and MVC,ORM structure design. <br /> - MySQL
              database.
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-sm-1 mt-3">
              <img src="/experience/report.png" className="img" />
            </div>
            <div className="col-sm-11 mt-3 discription">
              <b>
                Export Report: Create a function to export reports and place
                them in SFTP.
              </b>
              <br />
              <br /> - Develop the system using Node.js framework
            </div>
          </div>
        </div>
        <br />
        <br />
        <br />
        <div className="header-mian">
          Full-Time ( Full-Stack Developer )
          <span className="header" style={{ float: 'right' }}>
            Digio - Thailand Co., Ltd. <br />
            <span className="header" style={{ float: 'right' }}>
              May 2022 - Presen
            </span>
          </span>
        </div>
        <br />
        <br />
        <div className="content">
          <div className="row">
            <div className="col-sm-1">
              <img src="/experience/line.png" className="img" />
            </div>
            <div className="col-sm-11 discription">
              <b>
                Line Chat Bot: Create a Line Chat Bot system to issue contract
                documents. The system must receive information via the user's
                Chat to create a PDF document and be able to generate a download
                link.
              </b>
              <br />
              <br />- Design and develop the front-end web interface using
              Next.js(TypeScript), HTML, Javascript and CSS. <br /> - Develop
              the back-end using Next.js and Node.js.
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-sm-1">
              <img src="/experience/edoc.png" className="img" />
            </div>
            <div className="col-sm-11 discription">
              <b>
                E-document / E-bill Website: Develop a system for generating
                electronic documents, bills and invoices.
              </b>
              <br />
              <br /> - Design and develop the front-end web interface using
              Laravel, HTML, CSS, and JavaScript. <br /> - Develop backend
              system using Laravel and ORM structure design. <br /> - Manage
              work request lists using permission queues.
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-sm-1">
              <img src="/experience/Kleasings.png" className="img" />
            </div>
            <div className="col-sm-11 discription">
              <b>
                Web site Loan Car : Create a car loan system for Kasikorn Bank
                To make it easier for officials to consider and convenient for
                submitting various documents and following up on customer
                considerations.
              </b>
              <br />
              <br /> - Design and develop the front-end web interface using
              Next.js, Bootstrap, HTML, CSS and JavaScript. <br /> - Develop
              backend system using Next JS , Node JS and ORM structure design.
              <br /> - Create a robot to assist in system checks and testing
              using Cypress. <br /> - Develop the system with cybersecurity and
              sensitivity data standards compliant with banking regulations.
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-sm-1">
              <img src="/experience/Kleasings.png" className="img" />
            </div>
            <div className="col-sm-11 discription">
              <b>
                Web Portal Loan Car: Create a data management system that is
                necessary for the loan system and a system that supports
                customers
              </b>
              <br />
              <br /> - Design and develop the front-end web interface using
              Next.js, Bootstrap, HTML, CSS and JavaScript. <br /> - Develop
              backend system using Next JS , Node JS and ORM structure design.
              <br /> - Develop the system with cybersecurity and sensitivity
              data standards compliant with banking regulations.
            </div>
          </div>
        </div>
      </div>
    </ExperienceStyle>
  )
}

export default Experience
