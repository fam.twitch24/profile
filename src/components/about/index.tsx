const About = () => {
  function calculateAge(birthDate: string) {
    const today = new Date()
    const birthDateObj = new Date(birthDate)
    let age = today.getFullYear() - birthDateObj.getFullYear()
    const monthDiff = today.getMonth() - birthDateObj.getMonth()

    if (
      monthDiff < 0 ||
      (monthDiff === 0 && today.getDate() < birthDateObj.getDate())
    ) {
      age--
    }

    return age
  }
  return (
    <div className="mt-5" id="About">
      <div className="row">
        <div className="col-sm-6">
          <div className="title">ABOUT ME</div>
          <div className="mt-3" style={{ padding: '10px' }}>
            <div className="about-item"> Name : Natthaphong Phutla</div>
            <div className="about-item"> NickName : Fam (แฟ้ม)</div>
            <div className="about-item"> Birthday : 1999/11/24</div>
            <div className="about-item">Arg : {calculateAge('1999-11-24')}</div>
          </div>
        </div>
        <div className="col-sm-6">
          <div className="title">EDUCATION</div>
          <div
            className="mt-3"
            style={{ borderLeft: '2px solid #e6e6e6', padding: '10px' }}
          >
            <div className="about-item">
              <ul>
                <li>
                  Rajamangala university of technology isan 2018-2022
                  <br />
                  - Major : Computer Engineering <br />- GPA : 3.41
                </li>
                <li>
                  Mini Project (microcontroller) <br />- Create a system that
                  rotates a fan according to specified time and humidity levels.
                </li>
                <li>
                  Project (thesis) <br />- Advance Plan Management System for
                  Engineering Students.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default About
