import styled from 'styled-components'
const Skill = () => {
  const SkillStyle = styled.div`
    .img {
      width: 100%;
      transition: transform 0.5s ease;
      cursor: pointer;
      padding: 10px;
      display: flex;
      justify-content: center;
      align-items: center;
    }
    .img:hover {
      transform: scale(1.5);
    }
  `
  return (
    <SkillStyle>
      <div className="title mt-4 mb-3">SKILLS</div>
      <ul style={{ fontSize: '30px' }}>
        <li>HARD SKILL</li>
      </ul>
      <div className="row row-cols-2 row-cols-lg-6 g-2 g-lg-3">
        <div className="col">
          <img src="/skill/next.png" className="img" />
        </div>
        <div className="col">
          <img src="/skill/react.png" className="img" />
        </div>
        <div className="col">
          <img src="/skill/node.png" className="img" />
        </div>
        <div className="col">
          <img src="/skill/laravel.png" className="img" />
        </div>
        <div className="col">
          <img src="/skill/typeScript.png" className="img" />
        </div>
        <div className="col">
          <img src="/skill/html.png" className="img" />
        </div>
        <div className="col">
          <img src="/skill/php.png" className="img" />
        </div>
        <div className="col">
          <img src="/skill/Mysql.png" className="img" />
        </div>
        <div className="col">
          <img src="/skill/git.png" className="img" />
        </div>
      </div>
      <ul className="mt-5" style={{ fontSize: '30px' }}>
        <li>SOFT SKILL</li>
      </ul>
      <div className="row row-cols-2 row-cols-lg-6 g-2 g-lg-3">
        <div className="col">
          <img src="/skill/docker.png" className="img" />
        </div>
        <div className="col">
          <img src="/skill/aws.png" className="img" />
        </div>
        <div className="col">
          <img src="/skill/jenkin.png" className="img" />
        </div>
        <div className="col">
          <img src="/skill/cypress.png" className="img" />
        </div>
        <div className="col">
          <img src="/skill/c.png" className="img" style={{ width: '50%' }} />
        </div>
        <div className="col">
          <img src="/skill/redis.png" className="img" />
        </div>
      </div>
      <ul className="mt-5" style={{ fontSize: '30px' }}>
        <li>INTERESTED & LEARNING</li>
      </ul>
      <div className="row row-cols-2 row-cols-lg-6 g-2 g-lg-3">
        <div className="col">
          <img src="/skill/nest.png" className="img" />
        </div>
        <div className="col">
          <img src="/skill/go.png" className="img" />
        </div>
        <div className="col">
          <img src="/skill/devOps.png" className="img" />
        </div>
      </div>
    </SkillStyle>
  )
}

export default Skill
