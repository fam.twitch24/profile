import React, { useState } from 'react'
import { NavbarStyles } from './styles'
const Navbar = () => {
  const [route, setRoute] = useState('Profile')
  const onClick = (router: string) => {
    setRoute(router)
    const elm = document.getElementById(router)
    elm?.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest',
    })
  }
  return (
    <NavbarStyles>
      <nav className="navbar">
        <div className="container-fluid bar">
          <div
            onClick={() => {
              onClick('Profile')
            }}
            className={`item ${route === 'Profile' ? 'active-menu' : null}`}
          >
            Profile
          </div>
          <div
            onClick={() => {
              onClick('About')
            }}
            className={`item ${route === 'About' ? 'active-menu' : null}`}
          >
            About
          </div>
          <div
            onClick={() => {
              onClick('Skills')
            }}
            className={`item ${route === 'Skills' ? 'active-menu' : null}`}
          >
            Skills
          </div>
          <div
            onClick={() => {
              onClick('Experience')
            }}
            className={`item ${route === 'Experience' ? 'active-menu' : null}`}
          >
            Experience
          </div>
        </div>
      </nav>
    </NavbarStyles>
  )
}

export default Navbar
