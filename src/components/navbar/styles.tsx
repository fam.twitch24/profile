import styled from 'styled-components'

export const NavbarStyles = styled.div`
  .navbar {
    padding: 0px;
    margin-bottom: 20px;
    background: #fff;
    position: fixed;
    width: 100%;
  }
  .bar {
    width: 100%;
    padding: 10px;
    box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.2);
  }
  .item {
    display: inline;
    font-size: 20px;
    margin: auto;
    cursor: pointer;
    padding: 10px 30px 10px 30px;
    transition: background-color 0.9s;
    border-radius: 10px;
  }
  .item:hover {
    background-color: #a5a3a3;
    color: #fff;
  }
`
