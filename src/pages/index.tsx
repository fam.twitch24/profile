import styled from 'styled-components'
import { TypeAnimation } from 'react-type-animation'
import About from '../components/about'
import Skill from '../components/skill'
import Experience from '../components/experience'
const Index = () => {
  const MainStyles = styled.div`
    background: #f6f6f6;
    padding: 20px;
    border-radius: 15px;
    box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.2);
    .main {
      width: 100%;
    }
    .img {
      border-radius: 10px;
      transition: transform 0.5s ease;
      box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.2);
      cursor: pointer;
    }
    .img:hover {
      transform: scale(1.1);
    }
    .disciption {
      font-size: 18px;
      // display: flex;
      // justify-content: center;
      // align-items: center;
    }
    .name {
      margin-top: 10px;
      font-size: 50px;
    }
    .icons {
      font-size: 26px;
      margin-right: 10px;
    }
  `
  return (
    <>
      <div className="title mb-2" id="Profile">
        PROFILE
      </div>
      <MainStyles>
        <div className="row">
          <div className="col-sm-4 ">
            {/* <div className="status mb-3">
            <button className="btn btn-success" type="button" disabled>
              <span
                className="spinner-grow spinner-grow-sm"
                aria-hidden="true"
              ></span>
              &nbsp; กำลังมองหางาน
            </button>
          </div> */}
            <div className="main">
              <img className="img" src="/profile/profile.jpg" width={'100%'} />
            </div>
          </div>
          <div className="col-sm-8">
            <div className="name">NATTHAPHONG &nbsp; PHUTLA</div>
            <div className="position">
              <TypeAnimation
                sequence={[
                  // Same substring at the start will only be typed out once, initially
                  "I'am Developer",
                  1000, // wait 1s before replacing "Mice" with "Hamsters"
                  'My Position is Full-Stack Developer',
                  1000,
                ]}
                wrapper="span"
                speed={50}
                style={{ fontSize: '2em', display: 'inline-block' }}
                repeat={Infinity}
              />
            </div>
            <div className="text-secondary">
              <hr />
            </div>
            <div className="disciption">
              Highly skilled Full-Stack Developer with 2 years of experience in
              designing, developing, and deploying robust software solutions.
              Proficient in programming , with a strongfoundation in Node,
              Laravel, React , Next, ORM, etc. Specialized expertise in web
              development and various services, with a track record of
              delivering high-quality code on schedule.
            </div>
            <div className="contact">
              <div className="row mt-4">
                <div className="col-sm-6 mb-2">
                  <i
                    className="fa fa-phone-square icons"
                    aria-hidden="true"
                  ></i>{' '}
                  &nbsp; 093-327-7024
                </div>
                <div className="col-sm-6 mb-2">
                  <i className="fa fa-envelope icons" aria-hidden="true"></i>
                  &nbsp; fam.ddk@hotmail.com
                </div>
                <div className="col-sm-6 mb-2">
                  <span className="icons" style={{ fontSize: '18px' }}>
                    Line :
                  </span>
                  Fanclub1547
                </div>
                <div className="col-sm-6 mb-2">
                  <i className="fa fa-map-marker icons" aria-hidden="true"></i>{' '}
                  59/3 Nong Manao Sub-district, Kong District, Nakhon Ratchasima
                  Province, 30260
                </div>
              </div>
            </div>
          </div>
        </div>
      </MainStyles>
      <About />
      <Skill />
      <Experience />
    </>
  )
}
export default Index
