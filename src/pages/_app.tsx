// src/pages/_app.tsx
import { AppProps } from 'next/app'
import Navbar from '../components/navbar'
import 'bootstrap/dist/css/bootstrap.min.css'
import Head from 'next/head'
function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>NATTHAPHONG PHUTLA</title>
        <link
          href="https://fonts.googleapis.com/css2?family=Kanit&display=swap"
          rel="stylesheet"
        ></link>
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
        ></link>
      </Head>
      <style jsx global>
        {`
          body {
            font-family: 'Kanit', sans-serif;
            padding: 0px;
            margin: 0px;
            box-sizing: 0px;
            background: #f6f6f6;
          }
          .active-menu {
            transition: transform 0.5s ease;
            box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.2);
          }
          .title {
            font-size: 40px;
            font-weight: 700;
          }
        `}
      </style>
      <Navbar />
      <div
        className="container"
        style={{ padding: '80px 20px', background: '#fff' }}
      >
        <Component {...pageProps} />
      </div>
    </>
  )
}

export default MyApp
